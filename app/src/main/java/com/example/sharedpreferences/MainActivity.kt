package com.example.sharedpreferences

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    private lateinit var sharedPreferences: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
    }

    private fun init() {
        sharedPreferences = getSharedPreferences("data", MODE_PRIVATE)
        setdata()
    }

    fun saveData(view: View) {
        if (emailEditText.text.toString().isNotEmpty() && firstnameEditText.text.toString().isNotEmpty()
            && lastnameEditText.text.toString().isNotEmpty() && ageEditText.text.toString().isNotEmpty()
            && addressEditText.text.toString().isNotEmpty()) {
            val email = emailEditText.text.toString()
            val firstname = firstnameEditText.text.toString()
            val lastname = lastnameEditText.text.toString()
            val age = ageEditText.text.toString().toInt()
            val address = addressEditText.text.toString()

            val addData = sharedPreferences.edit()
            addData.putString("email", email)
            addData.putString("firstname", firstname)
            addData.putString("lastname", lastname)
            addData.putInt("age", age)
            addData.putString("address", address)
            addData.apply()
        }
        else {
            Toast.makeText(this, "please fill all required fields", Toast.LENGTH_SHORT).show()
        }
    }

    fun deleteData(view: View) {
        val deleteData = sharedPreferences.edit()
        deleteData.remove("email").commit()
        deleteData.remove("firstname").commit()
        deleteData.remove("lastname").commit()
        deleteData.remove("age").commit()
        deleteData.remove("address").commit()

        setdata()

    }

    private fun setdata() {
        val email = sharedPreferences.getString("email", "")
        val firstname = sharedPreferences.getString("firstname", "")
        val lastname = sharedPreferences.getString("lastname", "")
        val age = sharedPreferences.getInt("age", 0)
        val address = sharedPreferences.getString("address", "")

        emailEditText.setText(email)
        firstnameEditText.setText(firstname)
        lastnameEditText.setText(lastname)
        ageEditText.setText(age.toString())
        addressEditText.setText(address)
    }
}